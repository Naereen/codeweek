
=============================
Jour 4 – Compétitions de code
=============================

.. raw:: html

    Aujourd'hui, je <strike>vole</strike> m'inspire du travail d'Alexis Comte et Stéphane Henriot. Mais surtout, je casse le combo « …tion du/des … de … » de mes intitulés jusqu'à présent :

- `Résolution du quart de singe <1-quartdesinge.html>`_
- `Indexation des vidéos du Collège de France <2-indexation-cdf.html>`_
- `Exploration des rues de Paris <3-exploration-paris.html>`_

Initiation à la programmation
:::::::::::::::::::::::::::::

Hour of Code
============

Pendant cette semaine, il y a `Hour of Code <http://hourofcode.com/fr>`_. Il s'agit d'un mini-tutorial pour jouer avec des blocs et s'initier à la programmation. Alors que j'écris ces lignes, **68 millions** de gens ont essayé une heure de code, ce qui est un nombre plutôt raisonnable.

Leur dernière idée, que je trouve géniale : « `Apprends à coder avec Anna et Elsa de Frozen ! <http://code.org/frozen/>`_ »

Castor informatique
===================

Si vous êtes un enseignant au collège ou au lycée, vous serez sans doute intéressé par le `Castor informatique <http://castor-informatique.fr>`_, qui a lieu début novembre chaque année, et dont la dernière édition a attiré 228 324 élèves dont 49 % de filles. Voici un exemple d'exercice :

.. raw:: html

    <div class="well">
    Fais 51 à l'aide de cette calculette ne comportant que les boutons <strong>+1</strong> et <strong>×2</strong>. Essaie d'utiliser le moins de boutons possible.<br />
    <span style="font-size: 5em" id="number">0</span><br />
    <input type="button" class="btn btn-primary" value="+1" onclick="$('#number').text(parseInt($('#number').text()) + 1); $('#steps').text(parseInt($('#steps').text()) + 1); check()" />
    <input type="button" class="btn btn-primary" value="×2" onclick="$('#number').text($('#number').text() * 2); $('#steps').text(parseInt($('#steps').text()) + 1); check()" />
    <input type="button" class="btn" value="Recommencer" id="restart" onclick="$('#number').text(0); $('#steps').text(0); this.value='Recommencer';" /><br />
    Nombre de boutons utilisés : <strong id="steps">0</strong>
    </div>
    <script>
    function check() {
        if($('#number').text() == '51') {
            if($('#steps').text() == '9')
                $('#restart').val('Parfait !')
            else
                $('#restart').val('Tu peux faire mieux ! Recommencer')
        }
    }
    </script>

Cela permet de donner une idée de thèmes en informatique tels que la **représentation en binaire**, l'**optimisation** (plus court chemin), et ce sans que les élèves aient besoin de savoir coder. Courez donc voir `d'autres exemples d'exercices <http://castor-informatique.fr>`_ sur leur site !

Concours d'algorithmique
::::::::::::::::::::::::

Voici une liste non exhaustive de concours pour s'entraîner en programmation efficace, rédigée par nos soins.

`Codeforces <http://codeforces.com>`_
=====================================

- Python autorisé
- Excellents problèmes
- Concours réguliers
- Le plus complet (mathématiques, programmation dynamique, graphes, chaînes de caractères…)
- Possibilité en concours de hacker le code d'un autre
- Serveur instable
- Héberge concours annuels tels que CROC, Abby Cup
- Blogs actifs
- Téléchargement possible des sources des autres, après chaque épreuve

`CodeChef <http://codechef.com>`_
=================================

- Python autorisé… mais impossible de faire accepter autre chose que du C, du C# ou du Java
- Concours courts et longs, réguliers (mensuels)
- Combinatoire, géométrie, graphes, chaînes de caractères
- Forum pour discuter des problèmes
- Téléchargement possible des sources des autres, après chaque épreuve

`Prologin <http://prologin.org>`_
=================================

J'en ai déjà dit `beaucoup sur mon blog <http://vie.jill-jenn.net/2014/02/11/prologin-concours-national-informatique/>`_.

- Python autorisé
- Concours pour les -20 ans seulement
- Concours annuel (principalement national, assez bas niveau), avec demi-finale algo (papier et machine) et finale sur IA
- Entraînement libre
- Programmation dynamique, flots, graphes, tris...
- Forum (principalement utilisé par les russes, japonais et américains)

`HackerRank <http://hackerrank.com>`_
=====================================

- Python autorisé (une vingtaine de langages en tout)
- Des concours réguliers
- Un peu de diversité (algo, CodeGolf, AI, …) 
 
`France-ioi <http://www.france-ioi.org>`_
=========================================

- Cocorico
- Python autorisé
- Entraînements et concours principal pour les non bacheliers principalement
- Concours [Algoréa] bas niveau de temps en temps (bi-annuels ?)
- Cours/entraînement guidé
- Géométrie algorithmique, programmation dynamique, flots, graphes, tris...
- Système d'aide à la résolution

USACO
=====

- C/C++/JAVA/PASCAL
- Cours/entraînement guidé
- Concours mensuels avec Python autorisé ?

Google Code Jam
===============

- Tous langages autorisés
- Concours à plusieurs rounds une fois par an 
- 24 h pour le round de qualification, 2 h 30 pour le reste des rounds
- Téléchargement possible des sources des autres, après chaque épreuve
- Sans doute le « meilleur » concours d'algorithmique (d'excellents problèmes chaque année)

Facebook Hackercup
==================

- Tous langages autorisés
- Concours à plusieurs rounds une fois par an 
- Inspiré de Google Code Jam
- Il y a déjà eu problèmes de sécurité au niveau des soumissions, mais c'est parce que le concours est jeune
- Téléchargement possible des sources des autres, après chaque épreuve
- Très difficile de consulter les archives

TopCoder
========

- Python autorisé (depuis peu de temps)
- Nombreux thèmes de compétitions (Marathon Match, Algorithm Match, autres...)
- Utilisation d'une applet java obligatoire
- Site web austère
- Principalement problèmes à résolutions gloutonnes et de programmation dynamique (théorie de graphe, chaînes de caractères...)
- Hacks à la fin de la phase de code (problèmes résolus ou pas)
- Concours à plusieurs rounds une fois par an (OpenTCO)
- Téléchargement possible des sources des autres, après chaque épreuve

ProjectEuler
============

- Python autorisé (éxécution locale, tout autorisé, en fait)
- Nouveaux problèmes régulièrement
- Une seule instance par problème
- Différents challenges/classements (vitesse de résolution, résolution de suites particulières, etc.)
- Forum pour échanger avec les autres sur les problèmes résolus (ou simplement rire en lisant les codes J)

Défi Turing
===========

- Très fortement inspiré de ProjectEuler
- Francophone
- Assez simple pour l'instant, à destination des lycéens
- Pas encore de forum Un forum tout neuf !

Internet Problem Solving Contest
================================

- Concours annuel
- On peut participer seul ou en équipe (jusque 3 ?)
- De nombreux problèmes, TRÈS variés
- À peu près tout autorisé
- WTF

ACM-ICPC
========

- Payant, diverses contraintes sur l'âge/les études, il faut participer pour une école (pas plus de 3 équipes par école)
- C, C++ ou Java
- Droit à la doc et un poly de 25 pages
- Concours annuel
- Qualifications par zones (Europe du Sud Ouest pour nous), puis finale mondiale
- Haut niveau (concours mondial)
- Par équipes de 3 (un seul clavier)
- Problèmes très diffusés, voir par exemple ( https://icpcarchive.ecs.baylor.edu/ et http://acm.hit.edu.cn/ [et SPOJ, et UVA Online Judge, …])

UVA Online Judge
================

- C, C++, Java ou Pascal
- De nombreux problèmes, rangés par chapitres
- De nombreuses stats
- Concours parfois (live des ACM-ICPC, etc.)

Sphere Online Judge (SPOJ)
==========================

- Python autorisé (beaucoup de langages)
- De nombreux problèmes, plusieurs catégories
- Forum pour discuter de plein de choses

Codin'Game
==========

- Python autorisé (de plus en plus de langages)
- Plusieurs épreuves par an
- Aspect recrutement, possibilité de soumettre son profil à des entreprises (NINTENDO !!11), etc.
- Épreuves permanentes ?
- Évaluation rapide et relativement détaillée

Ressources
::::::::::

Skiena
======

http://www.cs.sunysb.edu/~skiena/392/

Stanford
========

http://www.stanford.edu/class/cs97si/

MIT
===

http://web.mit.edu/~ecprice/acm/notebook.pdf

École polytechnique
===================

- http://www.enseignement.polytechnique.fr/informatique/INF441-ACM/ (cours de Dürr)
- http://binetacm.wikidot.com/algorithmes (quelques trucs du même style, sous une forme un peu wiki)

Algorithmist
============

http://www.algorithmist.com/index.php/Main_Page

Autres liens
============

- http://lavergne.gotdns.org/CII/papers/TrainingICPC.pdf (Guide ICPC, voir les références)
- `The Hitchhiker's Guide to the Programming Contests <http://fr.scribd.com/doc/26546716/The-Hitchhiker%E2%80%99s-Guide-to-the-Programming-Contests>`_

.. raw:: html
    
    <div id="disqus_thread"></div>
    <script type="text/javascript">
        var disqus_shortname = 'jilljenn';
        (function() {
            var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
            dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
        })();
    </script>
    <noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
