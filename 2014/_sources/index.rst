
==============
JJ's Code Week
==============

Dans le cadre de la **semaine de l'enseignement de l'informatique** qui a lieu du lundi 8 au dimanche 14 décembre (`csedweek.org <http://csedweek.org>`_), chaque jour je présenterai :

* soit un code résolvant une tâche de la vie quotidienne ;
* soit une initiative liée à l'apprentissage de la programmation.

**Si vous ne savez pas coder,** je vous invite à tester gratuitement `une heure de code <http://hourofcode.com/fr>`_.

Toutes les pages sont sous licence `Creative Commons Zero 1.0 <http://creativecommons.org/publicdomain/zero/1.0/legalcode.fr>`_, ce qui fait que vous pouvez en faire ce que vous voulez, sans même me citer.

Déroulement de la semaine
=========================

.. toctree::
  :maxdepth: 1

  1-quartdesinge.rst
  2-indexation-cdf.rst
  3-exploration-paris.rst
  4-competitions.rst
  5-stages.rst
  6-questions-reponses.rst
  7-interactive-python.rst
